> [Internet of Things (IoT) | Training Course](2-lab-day-2.html) ▸ **Labs day 2**

## IoT Lab | Day two
In this second lab session, our objectives are:

1. to further interact with the device while covering important notions of polling, interrupts handling, ...
2. to connect the device to an Access Point via WiFi (with or without authentication, syncing time via NTP, ...)
3. to establish communication channels between devices via the LoRa modulation format (widely used in the IoT world)

### Prerequisites
Before going to the material and exercises, make sure to read and go through the following walkthrough guides:

1. [Setup](setup.html)
2. [Typical workflow](workflow.html) - especially the subfolder syncing section

### Lab session walkthrough(s)
1. [Detecting button pressure](button.html)
2. [WiFi](WiFi.html)
3. [LoRa MAC](lora-mac.html)

